import * as React from "react";
import search from "./search.png";
import BPL_Logo from "./BPL_Logo.jpg";
import "./App.css";
import { RouteComponentProps } from "@reach/router";

interface IProps extends RouteComponentProps {}

interface ISearchBar {
  inputValue: string;
  data: any[];
  playerData: any[];
  id: number;
}

class FootBall extends React.PureComponent<IProps, ISearchBar> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      inputValue: "",
      data: [],
      playerData: [],
      id: 0
    };

    let update = (
      data: never,
      event: React.FormEvent<HTMLInputElement>
    ): void => {
      const newValue = [] as any;

      this.setState({ data: newValue });
    };
  }

  resetTextField = (event: any) => {
    event.preventDefault();
    this.setState({ inputValue: "" });
  };

  componentDidMount() {}

  async getTeamPlayer(): Promise<void> {
    return fetch(
      `https://api-football-v1.p.rapidapi.com/v2/players/squad/${this.state.id}/2018-2019`,
      {
        method: "GET",
        headers: {
          "x-rapidapi-host": "api-football-v1.p.rapidapi.com",
          "x-rapidapi-key": "28250fa625msh756d97598c613bep1b1237jsn03187405ed89"
        }
      }
    )
      .then(response => {
        response.json().then(a => {
          let player = a.api.players.filter((b: any) => b.player_name);
          this.setState({ playerData: player });
          console.log(player);
        });
        console.log(response);
      })
      .catch(err => {
        console.log(err);
      });
  }

  handleSubmit = (event: React.SyntheticEvent) => {
    alert("Form has been submitted");
    console.log("this.state", this.state);
    event.preventDefault();
    fetch("https://api-football-v1.p.rapidapi.com/v2/teams/league/2", {
      method: "GET",
      headers: {
        "x-rapidapi-host": "api-football-v1.p.rapidapi.com",
        "x-rapidapi-key": "28250fa625msh756d97598c613bep1b1237jsn03187405ed89"
      }
    })
      .then(responseData => {
        responseData.json().then(x => {
          let team = x.api.teams.filter((a: any) =>
            a.name.toLowerCase().includes(this.state.inputValue.toLowerCase())
          );
          this.setState({ data: team });
          team.map((a: any) => {
            this.setState({ id: a.team_id });
          });
          console.log(team);
        });
      })
      .catch(error => {
        console.log(error);
      });
    console.log(this.state.id);

    this.getTeamPlayer();
  };

  ChangeHandler = (e: any) => {
    this.setState({
      inputValue: e.target.value
    });
  };

  render() {
    const { inputValue, data, playerData } = this.state;

    return (
      <form onSubmit={this.handleSubmit} method="post">
        <div>
          <img id="BPL_Logo_container" src={BPL_Logo} alt="BPL_Logo" />
        </div>
        <div>
          <header>
            <strong>
              <h5 id="BPL">
                Search for a team by typing in the name or the first three
                intials for each team. Search twice for results.
              </h5>
            </strong>
          </header>
        </div>

        <div id="container">
          <button id="button-holder" type="submit">
            <img id="image" src={search} alt="search" />
          </button>

          <input
            type="text"
            name="txtBox"
            id="input"
            placeholder="Search..."
            value={this.state.inputValue}
            spellCheck={false}
            onChange={this.ChangeHandler}
          />

          <button id="button-holder" type="reset" onClick={this.resetTextField}>
            Reset
          </button>

          <br />
          <div>
            <table key="table" id="table">
              <thead>
                <tr>
                  <th style={{ textAlign: "center" }} className="centered">
                    <h4>Players</h4>
                  </th>
                  <th style={{ textAlign: "center" }}>
                    <h4>ID</h4>
                  </th>
                  <th style={{ textAlign: "center" }}>
                    <h4>Name </h4>
                  </th>
                  <th style={{ textAlign: "center" }}>
                    <h4>Code</h4>
                  </th>
                </tr>
              </thead>

              <tbody id="td">
                {playerData.map((player, index) => {
                  return (
                    <tr>
                      <td>{player.player_name}</td>
                      {data.map((team, index) => {
                        return (
                          <>
                            <td>{team.team_id}</td>

                            <td>{team.name}</td>

                            <td>{team.code}</td>
                          </>
                        );
                      })}
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </form>
    );
  }
}

export default FootBall;
